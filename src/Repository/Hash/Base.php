<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Repository\Hash;

use ContextualCode\ContentImport\Entity\Hash\Base as Entity;
use ContextualCode\Crawler\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

abstract class Base extends ServiceEntityRepository
{
    protected function storeEntity(Entity $hash): void
    {
        $em = $this->getEntityManager();

        $em->persist($hash);
        $em->flush();
    }

    public function findByPage(Page $page): ?Entity
    {
        return $this->findOneBy([
            'identifier' => $page->getIdentifier(),
            'url' => $page->getUrl(),
        ]);
    }

    protected function removeEntityByPage(string $identifier, string $page, string $entityClass): void
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->delete($entityClass, 'h')
            ->where('h.identifier = :identifier AND h.url = :url')
            ->setParameter('identifier', $identifier)
            ->setParameter('url', $page);

        $qb->getQuery()->execute();
    }
}
