<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Repository\Hash;

use ContextualCode\ContentImport\Entity\Hash\Content as Entity;
use Doctrine\Persistence\ManagerRegistry;

class ContentRepository extends Base
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entity::class);
    }

    public function store(Entity $hash): void
    {
        $this->storeEntity($hash);
    }

    public function removeByPage(string $identifier, string $page): void
    {
        $this->removeEntityByPage($identifier, $page, Entity::class);
    }

    public function removeByContentIds(array $contentIds): void
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->delete(Entity::class, 'h')
            ->andWhere('h.contentId IN (:contentIds)')
            ->setParameter('contentIds', $contentIds);

        $qb->getQuery()->execute();
    }
}
