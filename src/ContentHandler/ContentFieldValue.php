<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\ContentHandler;

class ContentFieldValue implements ContentFieldValueInterface
{
    private $name;
    private $value;

    public function __construct(string $name, $value)
    {
        $this->setName($name);
        $this->setValue($value);
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setValue($value): void
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }
}
