<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200622180402 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cc_content_import_content_hashes (identifier VARCHAR(32) NOT NULL, url VARCHAR(736) NOT NULL, hash VARCHAR(32) NOT NULL, hash_attributes JSON DEFAULT NULL, extra_data JSON DEFAULT NULL, handler VARCHAR(256) NOT NULL, selector VARCHAR(1024) NOT NULL, content_id INT NOT NULL, content_version INT NOT NULL, type_id INT NOT NULL, is_root TINYINT(1) DEFAULT 0, INDEX content_id_idx (content_id), PRIMARY KEY(identifier, url)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_520_ci` ENGINE = InnoDB;');
        $this->addSql('CREATE TABLE cc_content_import_location_hashes (identifier VARCHAR(32) NOT NULL, url VARCHAR(736) NOT NULL, hash VARCHAR(32) NOT NULL, hash_attributes JSON DEFAULT NULL, location_id INT NOT NULL, is_redirect TINYINT(1) DEFAULT 0, INDEX location_id_idx (location_id), PRIMARY KEY(identifier, url)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_520_ci` ENGINE = InnoDB;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE cc_content_import_content_hashes');
        $this->addSql('DROP TABLE cc_content_import_location_hashes');
    }
}
