<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Command;

use ContextualCode\ContentImport\ContentHandler\ContentHandler;
use ContextualCode\ContentImport\ContentHandler\ContentOperationsInterface;
use ContextualCode\ContentImport\FieldHashTransformer\Base as FieldHashTransformer;
use ContextualCode\ContentImport\FieldTransformer\Base as FieldTransformer;
use ContextualCode\ContentImport\Service\Messages;
use ContextualCode\Crawler\Service\Handler as CrawlerHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Info extends Command
{
    protected static $defaultName = 'content-import:info';

    /** @var Messages */
    private $messages;

    /** @var ContentOperationsInterface */
    private $contentOperations;

    /** @var CrawlerHandler[] */
    private $crawlerHandlers;

    /** @var ContentHandler[] */
    private $contentHandlers;

    /** @var FieldTransformer[] */
    private $fieldTransformers;

    /** @var FieldHashTransformer[] */
    private $fieldHashTransformers;

    public function __construct(
        Messages $messages,
        ContentOperationsInterface $contentOperations,
        iterable $crawlerHandlers,
        iterable $contentHandlers,
        iterable $fieldTransformers,
        iterable $fieldHashTransformers
    ) {
        $this->messages = $messages;
        $this->contentOperations = $contentOperations;

        $this->crawlerHandlers = $this->getHandlers($crawlerHandlers, CrawlerHandler::class);
        $this->contentHandlers = $this->getHandlers($contentHandlers, ContentHandler::class);
        $this->fieldTransformers = $this->getHandlers($fieldTransformers, FieldTransformer::class);
        $this->fieldHashTransformers = $this->getHandlers($fieldHashTransformers, FieldHashTransformer::class);

        parent::__construct();
    }

    private function getHandlers(iterable $handlers, string $type): array
    {
        $return = [];

        foreach ($handlers as $handler) {
            if (!$handler instanceof $type) {
                continue;
            }

            $identifier = $type === CrawlerHandler::class ? $handler->getImportIdentifier() : $handler->getServiceIdentifier();

            $return[$identifier] = $handler;
        }

        if ($type === ContentHandler::class) {
            uasort($return, static function ($a, $b) {
                return $a->getPriority() <=> $b->getPriority();
            });
        }

        return $return;
    }

    protected function configure(): void
    {
        $this->setDescription($this->messages->get('command_info_description'));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $data = array_map(static function ($handler) {
            return [$handler->getImportIdentifier(), get_class($handler)];
        }, $this->crawlerHandlers);
        $this->table($io, 'crawler_handlers', ['identifier', 'class'], $data);

        $data = array_map(static function ($handler) {
            return [$handler->getPriority(), $handler->getServiceIdentifier(), get_class($handler)];
        }, $this->contentHandlers);
        $this->table($io, 'content_handlers', ['priority', 'identifier', 'class'], $data);

        $data = array_map(static function ($handler) {
            return [$handler->getServiceIdentifier(), get_class($handler)];
        }, $this->fieldTransformers);
        $this->table($io, 'field_transformers', ['identifier', 'class'], $data);

        $data = array_map(static function ($handler) {
            return [$handler->getServiceIdentifier(), get_class($handler)];
        }, $this->fieldHashTransformers);
        $this->table($io, 'field_hash_transformers', ['identifier', 'class'], $data);

        $this->table($io, 'content_operations_handler', ['class'], [[get_class($this->contentOperations)]]);

        return 0;
    }

    private function table(
        SymfonyStyle $io,
        string $titleIdentifier,
        array $headerIdentifiers,
        array $data
    ): void {
        $io->title($this->messages->get($titleIdentifier, [count($data)]));

        $headers = [];
        foreach ($headerIdentifiers as $headerIdentifier) {
            $headers[] = $this->messages->get($headerIdentifier);
        }

        $io->table($headers, $data);
    }
}
