<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Command;

use ContextualCode\ContentImport\Service\ContentImport;
use ContextualCode\ContentImport\Service\Messages;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Run extends Command
{
    protected static $defaultName = 'content-import:run';

    private const ARGUMENT_IDENTIFIER = 'identifier';
    private const OPTION_UPDATE = 'update';
    private const OPTION_UPDATE_LOCATIONS = 'update-locations';
    private const OPTION_OFFSET = 'offset';
    private const OPTION_LIMIT = 'limit';

    /** @var Messages */
    private $messages;

    /** @var ContentImport */
    private $import;

    public function __construct(Messages $messages, ContentImport $import)
    {
        $this->messages = $messages;
        $this->import = $import;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                self::ARGUMENT_IDENTIFIER,
                InputArgument::REQUIRED,
                $this->messages->get('command_argument_identifier')
            )
            ->addOption(
                self::OPTION_UPDATE,
                null,
                InputOption::VALUE_NONE,
                $this->messages->get('command_option_update')
            )
            ->addOption(
                self::OPTION_UPDATE_LOCATIONS,
                null,
                InputOption::VALUE_NONE,
                $this->messages->get('command_option_update_locations')
            )
            ->addOption(
                self::OPTION_OFFSET,
                null,
                InputOption::VALUE_OPTIONAL,
                $this->messages->get('command_option_offset')
            )
            ->addOption(
                self::OPTION_LIMIT,
                null,
                InputOption::VALUE_OPTIONAL,
                $this->messages->get('command_option_limit')
            )
            ->setDescription($this->messages->get('command_run_description'));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $identifier = $input->getArgument(self::ARGUMENT_IDENTIFIER);
        $update = $input->getOption(self::OPTION_UPDATE);
        $updateLocations = $input->getOption(self::OPTION_UPDATE_LOCATIONS);

        $offset = $input->getOption(self::OPTION_OFFSET);
        if ($offset !== null) {
            $offset = (int) $offset;
        }
        $limit = $input->getOption(self::OPTION_LIMIT);
        if ($limit !== null) {
            $limit = (int) $limit;
        }

        $message = $this->messages->get('command_live_logs', [$this->import->getLogFilepath()]);
        $io->block($message, null, 'fg=black;bg=yellow', ' ', true);

        $io->title($this->messages->get('command_import_content_title'));
        $progressBar = $this->getProgressBar($io);
        $progressBar->start(1);
        $this->import->setProgressBar($progressBar);
        $this->import->importPages($identifier, $update, $updateLocations, $offset, $limit);
        $progressBar->finish();
        $io->newLine(2);

        $io->title($this->messages->get('command_import_redirects_title'));
        $progressBar = $this->getProgressBar($io);
        $progressBar->start(1);
        $this->import->setProgressBar($progressBar);
        $this->import->importRedirects($identifier);
        $progressBar->finish();
        $io->newLine(2);

        $message = $this->messages->get('command_run_success', $this->import->getStatistics());
        $io->block($message, null, 'fg=black;bg=green', ' ', true);

        return 0;
    }

    protected function getProgressBar(SymfonyStyle $io): ProgressBar
    {
        ProgressBar::setFormatDefinition('custom', '%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');

        $progressBar = $io->createProgressBar();
        $progressBar->setFormat('custom');

        return $progressBar;
    }
}
