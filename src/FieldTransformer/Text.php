<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValue;
use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\Crawler\Entity\Page;

class Text extends Html
{
    public function getServiceIdentifier(): string
    {
        return 'text';
    }

    public function getFieldValue(
        Page $page,
        string $fieldName,
        array $params = []
    ): ContentFieldValueInterface {
        $element = $this->getElementBySelector($page, $params);
        $text = trim((string) $element->nodeValue);

        $value = new ContentFieldValue($fieldName, $text);
        $this->checkValueIsRequired($params, $value);

        return $value;
    }
}
