<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldTransformer;

class Image extends File
{
    public function getServiceIdentifier(): string
    {
        return 'image';
    }
}
