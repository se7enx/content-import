<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValue;
use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\ContentImport\Service\ContentImport;
use ContextualCode\Crawler\Entity\Page;
use DOMElement;

class Html extends Base
{
    public const PARAM_SELECTOR = 'selector';

    public function getServiceIdentifier(): string
    {
        return 'html';
    }

    public function getFieldValue(
        Page $page,
        string $fieldName,
        array $params = []
    ): ContentFieldValueInterface {
        $element = $this->getElementBySelector($page, $params);
        $html = trim($element->ownerDocument->saveXML($element));

        $value = new ContentFieldValue($fieldName, $html);
        $this->checkValueIsRequired($params, $value);

        return $value;
    }

    protected function getElementBySelector(Page $page, array $params): DOMElement
    {
        $selector = $this->getRequiredParameter($params, self::PARAM_SELECTOR);

        if (
            isset($params[ContentImport::PARAM_CONTENT_ITEM_SELECTOR])
            && $params[ContentImport::PARAM_CONTENT_ITEM_SELECTOR]
        ) {
            $selector = $params[ContentImport::PARAM_CONTENT_ITEM_SELECTOR] . '/' . $selector;
        }

        return $this->selectElement($page, $selector);
    }
}
