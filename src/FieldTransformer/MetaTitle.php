<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValue;
use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\ContentImport\Exception\InvalidContentField;
use ContextualCode\Crawler\Entity\Page;

class MetaTitle extends Html
{
    public function getServiceIdentifier(): string
    {
        return 'meta-title';
    }

    public function getFieldValue(
        Page $page,
        string $fieldName,
        array $params = []
    ): ContentFieldValueInterface {
        try {
            $element = $this->selectElement($page, '//title');
            $title = trim(preg_replace('/\s+/', ' ', (string) $element->nodeValue));
        } catch(InvalidContentField $e) {
            $title = null;
        }

        $value = new ContentFieldValue($fieldName, $title);
        $this->checkValueIsRequired($params, $value);

        return $value;
    }
}
