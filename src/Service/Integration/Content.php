<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Service\Integration;

use ContextualCode\ContentImport\ContentHandler\ContentInterface;
use ContextualCode\ContentImport\ContentHandler\ContentTypeInterface;
use ContextualCode\ContentImport\ContentHandler\LocationInterface;

class Content implements ContentInterface
{
    private $id;
    private $version;
    private $contentTypeId;
    private $mainLocationId;

    public function __construct(int $id, int $version = 1, $contentTypeId = 1, $mainLocationId = 1)
    {
        $this->id = $id;
        $this->version = $version;
        $this->contentTypeId = $contentTypeId;
        $this->mainLocationId = $mainLocationId;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return 'Content name';
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function getContentType(): ContentTypeInterface
    {
        return new ContentType($this->contentTypeId, 'content-type');
    }

    public function getMainLocation(): LocationInterface
    {
        return new Location($this->mainLocationId, $this->id);
    }
}
