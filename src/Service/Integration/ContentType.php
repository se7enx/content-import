<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Service\Integration;

use ContextualCode\ContentImport\ContentHandler\ContentTypeInterface;

class ContentType implements ContentTypeInterface
{
    private $id;
    private $identifier;

    public function __construct(int $id, string $identifier)
    {
        $this->id = $id;
        $this->identifier = $identifier;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }
}
