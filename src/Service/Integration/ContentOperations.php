<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Service\Integration;

use ContextualCode\ContentImport\ContentHandler\ContentHandler;
use ContextualCode\ContentImport\ContentHandler\ContentInterface;
use ContextualCode\ContentImport\ContentHandler\PageContentItem;
use ContextualCode\ContentImport\ContentHandler\ContentOperationsInterface;
use ContextualCode\ContentImport\ContentHandler\LocationInterface;
use ContextualCode\ContentImport\Exception\ContentNotFound;
use ContextualCode\ContentImport\Exception\LocationNotFound;
use Psr\Log\LoggerInterface;

class ContentOperations implements ContentOperationsInterface
{
    /** @var LoggerInterface */
    protected $logger;

    public function __construct(LoggerInterface $contentImportLogger)
    {
        $this->logger = $contentImportLogger;
    }

    public function loadContent(int $id): ContentInterface
    {
        $context = ['content_id' => $id];

        $this->logger->critical('Loading content', $context);

        if (rand(0, 1) === 1) {
            throw new ContentNotFound('Content not found', $context);
        }

        return new Content($id, rand(1, 10), rand(1, 20), rand(1, 100));
    }

    public function loadLocation(int $id): LocationInterface
    {
        $context = ['location_id' => $id];

        $this->logger->critical('Loading location', $context);

        if (rand(0, 1) === 1) {
            throw new LocationNotFound('Location not found', $context);
        }

        return new Location($id, rand(1, 100));
    }

    public function createContent(
        PageContentItem $contentItem,
        ContentHandler $handler,
        array $fields
    ): ContentInterface {
        $this->logger->critical('Creating new content', ['url' => $contentItem->getPage()->getUrl()]);

        return new Content(rand(1, 100), rand(1, 10), rand(1, 20), rand(1, 100));
    }

    public function updateContent(
        ContentInterface $content,
        PageContentItem $contentItem,
        ContentHandler $handler,
        array $fields
    ): ContentInterface {
        $this->logger->critical('Updating new content', ['url' => $contentItem->getPage()->getUrl()]);

        return $content;
    }

    public function createNewLocation(
        ContentInterface $content,
        PageContentItem $contentItem,
        ContentHandler $handler
    ): ?LocationInterface {
        $this->logger->critical('Updating new location', ['url' => $contentItem->getPage()->getUrl()]);

        return null;
    }

    public function createRedirect(
        int $locationId,
        PageContentItem $contentItem,
        ContentHandler $handler
    ): bool {
        $this->logger->critical('Updating new redirect', ['url' => $contentItem->getPage()->getUrl()]);

        return rand(0, 1) === 0;
    }

    public function postImport(ContentInterface $content): void
    {
        $this->logger->critical('Post import handler', ['content_id' => $content->getId()]);
    }

    public function preTransform(PageContentItem $contentItem, ContentHandler $handler): void
    {
    }

    public function postTransform(PageContentItem $contentItem, ContentHandler $handler): void
    {
    }
}
