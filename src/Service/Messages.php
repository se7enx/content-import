<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Service;

use RuntimeException;

class Messages
{
    protected $messages = [
        'command_run_description' => 'Imports the content from previous crawler results',
        'command_set_root_hashes_description' => 'Sets content and location import hashes for specified existing root location',
        'command_info_description' => 'Provides information about available crawlers and content handlers',
        'command_argument_identifier' => 'The crawler identifier, needs to be defined in your app',
        'command_argument_location_id' => 'Root location id',
        'command_option_update' => 'Updates modified pages if this option is provided',
        'command_option_update_locations' => 'Updates locations for modified pages if this option is provided',
        'command_option_offset' => 'Offset used to fetch the pages',
        'command_option_limit' => 'Limit used to fetch the pages',
        'command_live_logs' => 'To get live logs, please run the following command in a new terminal:

tail -f %s',
        'command_import_content_title' => 'Importing the content ...',
        'command_import_redirects_title' => 'Importing custom URL aliases (redirects) ...',
        'command_run_success' => 'Content import completed:

 * %d new content items were created
 * %d existing content items were updated
 * %d new locations were assigned
 * %d url redirects were created',
        'command_set_root_hashes_success' => 'Import hashes were set for "%s" (contentId: %d, locationId: %d)',
        'error_required_param_missing' => 'Required "%s" parameter is missing',
        'error_required_field_empty' => 'Required value can not be empty for "%s" field',
        'error_response_to_dom_xpath' => 'Unable to convert response to DOMXPath',
        'error_unable_download_file' => 'Unable to download "%s" into "%s"',
        'error_create_local_file' => 'Unable to create local file in %s',
        'error_path_is_not_writable' => '"%s" is not writable',
        'error_no_referer' => 'Unable to get referer page for %s',
        'error_no_dom_element' => 'Unable to extract element "%s"',
        'error_invalid_dom_element' => 'Invalid element',
        'error_content_handler_not_found' => 'Unable to get import content handler for page %s',
        'error_field_transformer_no_id' => 'Required "id" key is missing in the field transformer definition',
        'error_field_hash_transformer_no_id' => 'Required "id" key is missing in the field hash transformer definition',
        'error_field_transformer_not_found' => 'Field transformer "%s" is not available',
        'error_field_hash_transformer_not_found' => 'Field hash transformer "%s" is not available',
        'error_no_final_destination_page' => 'Unable to get final destination page for %s redirect',
        'skip_content_update' => 'Skipping update for content #%d, page url %s',
        'content_already_exist' => 'Content %d already exist for %s',
        'content_was_removed' => 'Content %d not found, recreating it from %s',
        'content_hash_version_mismatch' => 'Unable to update content #%d because it was manually modified (current published version: %d, import hash version: %d)',
        'crawler_handlers' => 'Crawler handlers (%d):',
        'content_handlers' => 'Content import handlers (%d):',
        'field_transformers' => 'Field transformers (%d):',
        'field_hash_transformers' => 'Field hash transformers (%d):',
        'content_operations_handler' => 'Content operations handler:',
        'priority' => 'Priority',
        'identifier' => 'Identifier',
        'class' => 'Class',
    ];

    public function get(string $identifier, array $params = []): string
    {
        if (!isset($this->messages[$identifier])) {
            throw new RuntimeException(sprintf('Unable to find "%s" message', $identifier));
        }

        $message = $this->messages[$identifier];

        return count($params) ? vsprintf($message, $params) : $message;
    }
}
