<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Entity\Hash;

abstract class Base
{
    protected $identifier;
    protected $url;
    protected $hash;
    protected $hashAttributes;

    public function __construct(string $identifier, string $url)
    {
        $this->identifier = $identifier;
        $this->url = $url;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function getHashAttributes(): array
    {
        return $this->hashAttributes;
    }

    public function setIdentifier(string $identifier): void
    {
        $this->identifier = $identifier;
    }

    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }

    public function setHashAttributes(array $hashAttributes, bool $updateHash = false): void
    {
        ksort($hashAttributes);

        $this->hashAttributes = $hashAttributes;

        if ($updateHash || empty($this->getHash())) {
            $this->setHash(self::calculateHash($hashAttributes));
        }
    }

    public function isTheSame(array $attributes): bool
    {
        return $this->getHash() === self::calculateHash($attributes);
    }

    public static function areTheHashesSame(array $oldAttributes, array $newAttributes): bool
    {
        return self::calculateHash($oldAttributes) === self::calculateHash($newAttributes);
    }

    protected static function calculateHash(array $attributes): string
    {
        ksort($attributes);

        return md5(serialize($attributes));
    }
}
