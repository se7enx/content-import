<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Entity\Hash;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ContextualCode\ContentImport\Repository\Hash\LocationRepository")
 * @ORM\Table(name="cc_content_import_location_hashes",indexes={@ORM\Index(name="location_id_idx", columns={"location_id"})})
 */
class Location extends Base
{
    /**
     * @ORM\Column(type="string", length=32)
     * @ORM\Id
     */
    protected $identifier;

    /**
     * @ORM\Column(type="string", length=736)
     * @ORM\Id
     */
    protected $url;

    /** @ORM\Column(type="string", length=32) */
    protected $hash;

    /** @ORM\Column(type="json", nullable=true) */
    protected $hashAttributes;

    /** @ORM\Column(type="integer", length=11) */
    private $locationId;

    /** @ORM\Column(type="boolean", nullable=true, options={"default": false}) */
    private $isRedirect = false;

    public function getLocationId(): ?int
    {
        return $this->locationId;
    }

    public function isRedirect(): bool
    {
        return $this->isRedirect;
    }

    public function setLocationId(int $locationId): void
    {
        $this->locationId = $locationId;
    }

    public function setRedirect(bool $isRedirect): void
    {
        $this->isRedirect = $isRedirect;
    }
}
